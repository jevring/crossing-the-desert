import java.util.ArrayList;
import java.util.LinkedList;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: Markus Jevring
 * Date: 2007-mar-18
 * Time: 17:03:44
 */
public class CrossingTheDesert {
    private static int MAX_FOOD = 1000000;
    private LinkedList<State> states;   // contains all states where currentLocation ISN'T the starting point
                                        // for starting states, use .addAtEnd()
    private ArrayList<State> successfulWalks;
    private int numberOfLocations;
    private int capacity;
    private ArrayList<Location> locations;
    private int trials = 0;
    private double bestGuessSoFar = MAX_FOOD;

    public static void main(String[] args) {
        ArrayList<Location> l = new ArrayList<Location>();
        l.add(new Location(10, -20));
        l.add(new Location(-10, 5));
        l.add(new Location(30, 15));
        l.add(new Location(15, 35));
        CrossingTheDesert ctd = new CrossingTheDesert(4, 100, l);
        long t0 = System.currentTimeMillis();
        ctd.walk();
        long tt = System.currentTimeMillis() - t0;
        System.out.println("Time taken: " + tt);
/*

        String line;
        boolean acceptingCoordinates = false;
        int numberOfLocations = 0;
        int capacity = 0;
        ArrayList<Location> locations = new ArrayList<Location>();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        try {
            while ((line = in.readLine()) != null)  {
                // read the number of states,
                // for that namy lines, read new locations
                // when we have the locations, start walking...
                if (!acceptingCoordinates && !line.equals("0 0")) {
                    acceptingCoordinates = true;
                    String[] t = line.split(" ");
                    numberOfLocations = Integer.parseInt(t[0]) + 1;
                    capacity = Integer.parseInt(t[1]);
                } else if(acceptingCoordinates && locations.size() < numberOfLocations) {
                    String[] t = line.split(" ");
                    int x = Integer.parseInt(t[0]);
                    int y = Integer.parseInt(t[1]);
                    locations.add(new Location(x,y));
                } else if (acceptingCoordinates && locations.size() == numberOfLocations) {
                    // we've filledone test set, lets create it.
                    CrossingTheDesert ctd = new CrossingTheDesert(numberOfLocations, capacity, locations);
                    ctd.walk();
                    acceptingCoordinates = false;
                    numberOfLocations = capacity = 0;
                    locations = new ArrayList<Location>();
                } else if (!acceptingCoordinates && line.equals("0 0")){
                    return;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            System.out.println("Do not enter values that are not numbers.");
        }
*/
    }

    public CrossingTheDesert(int numberOfLocations, int capacity, ArrayList<Location> locations) {
        this.numberOfLocations = numberOfLocations;
        this.capacity = capacity;
        this.locations = locations;
        states = new LinkedList<State>();
        successfulWalks = new ArrayList<State>();
    }

    public void walk() {
        State s = new State(0, 0, new double[numberOfLocations]);
        // for each state in the list, expand that state.
        states.addLast(s);

        while (states.size() > 0) {
            // repeadedly take the first state and expand it until we reach the set of no more states
//            System.out.println("states.size(): " + states.size());
            addStatesToList(
                    expandState(states.remove())
            );
        }

        if (successfulWalks.size() > 0) {
            System.out.println("Found " + successfulWalks.size() + " possible ways of making it to the end.");
            State bestState = null;
            for (int i = 0; i < successfulWalks.size(); i++) {
                State state = successfulWalks.get(i);
                if (bestState == null) {
                    bestState = state;
                } else {
                    if (state.getFoodTaken() < bestState.getFoodTaken()) {
                        bestState = state;
                    }
                }
            }
            System.out.println("Trial " + (trials++) + ": " + (int)Math.ceil(bestState.getFoodTaken() - bestState.getFoodAtAllLocations()[numberOfLocations-1]) + " units of food.");
        } else {
            System.out.println("Trial " + (trials++) + ": Impossible");
        }
    }

    private ArrayList<State> expandState(State state) {
        ArrayList<State> expandedStates = new ArrayList<State>();
        if (state.getFoodTaken() > MAX_FOOD) {
            return expandedStates;
        } else if (state.getFoodTaken() > bestGuessSoFar) { // without this block, then the sample input finds 14708 ways of making it to the end. This terminates two long branches, and brings execution time down from 438 to 15 milliseconds :D
            System.out.println("breaking branch due to excessive food consumption");
            return null;
/*
        } else if (state.getCurrentLocation() == (numberOfLocations - 1)) {
            // just to check
            System.out.println("We got to the end, in a weird way"); // note that we should never actually end up here!
            successfulWalks.add(state);
            return expandedStates;
*/
        } else if(state.getFoodAtCurrentLocation() == 0 && state.getCurrentLocation() != 0) {
            return expandedStates;
        } else {
            // this is where we actually expand the state.
            Location currentLocation = locations.get(state.getCurrentLocation());

            for (int i = 0; i < locations.size(); i++) {
                if (i == state.getCurrentLocation()) {
                    continue; // don't try to walk to the destination we are already at. (horrible grammar)
                }
                Location location = locations.get(i);
                double distanceToNextLocation = currentLocation.getDistanceTo(location);
                if ((distanceToNextLocation*2) <= capacity) {
                    // this means it's actually reachable
                    // check if we have enough food to reach if
                    if (distanceToNextLocation <= state.getFoodAtCurrentLocation() || state.getCurrentLocation() == 0) { // if we're at the start, it doesn't matter how much food we have, we'll just pick up some more
                        // the destination is reachable, create a new state for going there.
                        // check if this is the destination. if it is, go there with all the remaining food at this node and stop expanding the tree
                        if (i == (numberOfLocations - 1)) {
                            State endingState = new State(state.getFoodTaken(), (numberOfLocations - 1), state.getFoodAtAllLocations());
                            endingState.setFoodAtLocation(state.getCurrentLocation(), 0); // take all the food from the last step with us, so we can discount that when we reach the end
                            endingState.setFoodAtLocation((numberOfLocations - 1), state.getFoodAtLocation(state.getCurrentLocation()) - distanceToNextLocation); // all the food we brought minus what we needed to walk
                            successfulWalks.add(endingState);

                            double resourcesUsed = endingState.getFoodTaken() - endingState.getFoodAtAllLocations()[numberOfLocations-1];
                            if (resourcesUsed < bestGuessSoFar) {
                                System.out.println("setting best so far: " + resourcesUsed);
                                bestGuessSoFar = resourcesUsed;
                            }
                            return null;
                        }
 
                        // actually, create a new state for going there with a) just the necessary amount, and b) as much as we can possibly bring with ut
                        State locationReachedWithMinimalFood = new State(state.getFoodTaken(), i, state.getFoodAtAllLocations());
                        State locationReachedWithMaximalFood = new State(state.getFoodTaken(), i, state.getFoodAtAllLocations());
                        double foodTakenFromThisLocation = Math.min((capacity - distanceToNextLocation), state.getFoodAtCurrentLocation()); // only take as much as we can
                        double foodAtCurrentLocation = state.getFoodAtCurrentLocation();

                        if (state.getCurrentLocation() == 0) {
                            // this means we're (back) at the starting state.
                            // - update the food taken for the two states
                            locationReachedWithMaximalFood.setFoodTaken(state.getFoodTaken() + (capacity - distanceToNextLocation)); // account for water to get to the next state
                          locationReachedWithMinimalFood.setFoodTaken(state.getFoodTaken() + distanceToNextLocation);
                            // - update the amount of food in the starting place for each state
                            locationReachedWithMaximalFood.setFoodAtLocation(0, foodAtCurrentLocation + (capacity - distanceToNextLocation));
                            locationReachedWithMinimalFood.setFoodAtLocation(0, foodAtCurrentLocation + distanceToNextLocation);
                            foodTakenFromThisLocation = (capacity - distanceToNextLocation);
                            foodAtCurrentLocation = (capacity - distanceToNextLocation); 

                        }
                        // - take food from the location we're moving from
                        locationReachedWithMaximalFood.setFoodAtLocation(state.getCurrentLocation(), foodAtCurrentLocation - foodTakenFromThisLocation );
                        locationReachedWithMinimalFood.setFoodAtLocation(state.getCurrentLocation(), foodAtCurrentLocation - distanceToNextLocation);

                        // - update the amount of food at the location we're moving to, which is the amount of food we left with minus what we consumed on the way
                        locationReachedWithMaximalFood.setFoodAtLocation(i, state.getFoodAtLocation(i) + (foodTakenFromThisLocation - distanceToNextLocation)); // add what was left over after the walk
                        locationReachedWithMinimalFood.setFoodAtLocation(i, state.getFoodAtLocation(i)); // this remains the same, because we walked here with just as much food as we needed, and nothing extra.

                        // - add these states to the list
                        expandedStates.add(locationReachedWithMaximalFood);
                        expandedStates.add(locationReachedWithMinimalFood);
                    }
                }
            }
        }
        return expandedStates;
    }

    /**
     * Checks which states have us in the starting positon and adds them to the END of the list.
     * Checks which states have us in any other position and adds them to the beginning of the list.
     * @param expandedStates the list of expandedStates;
     */
    private void addStatesToList(ArrayList<State> expandedStates) {
        if (expandedStates != null && expandedStates.size() > 0) {
            for (State state : expandedStates) {
                if (state.getCurrentLocation() == 0) {
                    states.addLast(state);
                } else {
                    states.addFirst(state);
                }
            }
        }

    }
}
