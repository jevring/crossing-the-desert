/**
 * Created by IntelliJ IDEA.
 * User: Markus Jevring
 * Date: 2007-mar-18
 * Time: 17:06:23
 */
public class Location {
    public int x;
    public int y;

    public Location(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public double getDistanceTo(Location l) {
        double d = Math.sqrt(Math.pow((l.x-x), 2) + Math.pow((l.y-y), 2));
//        System.out.println("Distance between (" + x + "," + y + ") and (" + l.x + "," + l.y + ") is: " + d);
        return d;
    }
}
