/**
 * Created by IntelliJ IDEA.
 * User: Markus Jevring
 * Date: 2007-mar-18
 * Time: 17:03:53
 */
public class State {
    private double foodTaken;
    private int myCurrentLocation;
    private double[] foodAtAllLocations;
    private String path = "";


    public State(double foodTaken, int myCurrentLocation, double[] food) {
        this.foodTaken = foodTaken;
        this.myCurrentLocation = myCurrentLocation;
        this.foodAtAllLocations = food;
        path += myCurrentLocation;
    }

    public double getFoodTaken() {
        return foodTaken;
    }

    public void setFoodTaken(double foodTaken) {
        this.foodTaken = foodTaken;
    }

    public int getCurrentLocation() {
        return myCurrentLocation;
    }

    public double[] getFoodAtAllLocations() {
        return foodAtAllLocations.clone();
    }

    public double getFoodAtLocation(int location) {
        return foodAtAllLocations[location];
    }

    public double getFoodAtCurrentLocation() {
        return foodAtAllLocations[myCurrentLocation];
    }

    public void setFoodAtLocation(int location, double food) {
        this.foodAtAllLocations[location] = food;
    }

    public String getPath() {
        return path;
    }
}
